import 'package:favorite_places/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';

final colorScheme = ColorScheme.fromSeed(
  brightness: Brightness.light,
  seedColor: const Color.fromARGB(255, 6, 191, 247),
);

final theme = ThemeData().copyWith(
  scaffoldBackgroundColor: colorScheme.surface,
  colorScheme: colorScheme,
  textTheme: GoogleFonts.sofiaTextTheme().copyWith(
    titleSmall: GoogleFonts.sofia(
      fontWeight: FontWeight.bold,
    ),
    titleMedium: GoogleFonts.sofia(
      fontWeight: FontWeight.bold,
    ),
    titleLarge: GoogleFonts.sofia(
      fontWeight: FontWeight.bold,
    ),
  ),
);

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Great Places',
      theme: theme,
      home: const HomeScreen(),
    );
  }
}
