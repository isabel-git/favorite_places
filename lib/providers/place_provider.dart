import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:favorite_places/models/place_model.dart';

// Define a class to hold the state
class PlaceNotifier extends StateNotifier<List<Place>> {
  PlaceNotifier() : super([]);

  void addPlace(String title) {
    final newPlace = Place(title: title);
    state = [newPlace, ...state];
  }
}

// Create a provider for the PlaceNotifier
final placeProvider = StateNotifierProvider<PlaceNotifier, List<Place>>((ref) {
  return PlaceNotifier();
});
