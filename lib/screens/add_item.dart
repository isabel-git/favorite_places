import 'package:favorite_places/widgets/image_imput.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:favorite_places/providers/place_provider.dart';

class AddItemScreen extends ConsumerWidget {
  const AddItemScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final placeNotifier = ref.read(placeProvider.notifier);
    final titleController = TextEditingController();

    void savePlace() {
      final enteredText = titleController.text;

      //convalida di base
      if (enteredText.isEmpty) {
        return;
      }

      placeNotifier.addPlace(enteredText);
      Navigator.pop(context);
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('New place'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          children: [
            TextField(
              controller: titleController,
              maxLength: 50,
              decoration: const InputDecoration(label: Text('Title')),
            ),
            const SizedBox(height: 12),
            const ImageImput(),
            const SizedBox(height: 12),
            ElevatedButton(
              onPressed: savePlace,
              child: const Text('Add'),
            ),
          ],
        ),
      ),
    );
  }
}
